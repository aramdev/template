'use strict'

const gulp = require('gulp');
const gaze = require('gaze');


const from = 'src/assets';
const to = 'dist/assets';

function lazyRequireTask(taskName, path, options){
    options = options || {}; 
    options.taskName = taskName;
    gulp.task(taskName, function(callback){
        let task = require(path).call(this, options);
        return task(callback)
    })
}

/************GET MEDIA START************/

lazyRequireTask('ico', './tasks/ico.js', {
    src: `${from}/ico/ico.png`,
    dest: `${to}/ico`
});

lazyRequireTask('img', './tasks/retinaze.js', {
    src: `${from}/img/**/*.{png,jpg,gif}`,
    dest: `${to}/img`
});

lazyRequireTask('img:svg', './tasks/copy.file.js', {
    src: `${from}/img/**/*.svg`,
    dest: `${to}/img`
});

lazyRequireTask('pictures', './tasks/retinaze.js', {
    src: `${from}/pictures/**/*.{png,jpg,gif}`,
    dest: `${to}/pictures`
});

lazyRequireTask('pictures:svg', './tasks/copy.file.js', {
    src: `${from}/pictures/**/*.svg`,
    dest: `${to}/pictures`
});

lazyRequireTask('logos', './tasks/copy.file.js', {
    src: `${from}/logos/**/*`,
    dest: `${to}/pictures`
});


lazyRequireTask('media', './tasks/copy.file.js', {
    src: `${from}/media/**/*`,
    dest: `${to}/media`
});

lazyRequireTask('sprite:svg', './tasks/sprites.svg.js', {
    src: `${from}/sprites-svg/*.svg`,
    dest: `${to}`
});

lazyRequireTask('sprite:clean', './tasks/sprite.clean.js', {
    del: [
        'dist/assets/img/sprite-*.svg' 
    ],
});

lazyRequireTask('fonts:local', './tasks/copy.file.js', {
    src: `${from}/fonts/**/*`,
    dest: `${to}/fonts`
});


lazyRequireTask('fonts:icons', './tasks/icons.js', {
    src: `${from}/icons/*.svg`,
    dest: `${to}/fonts`,
    path: `template.fonts.icons/_fonts.icons.scss`,
});

gulp.task('app:media', 
    gulp.parallel(
		'ico',
		'img',
		'img:svg',
        'sprite:svg',
        'pictures',
		'pictures:svg',
        'media',
        'logos',
        'fonts:icons',
        'fonts:local',
    ) 
);

/************************/

/*************GET STYLES START*************/
lazyRequireTask('lib:styles', './tasks/sass.lib.js', {
    src: `${from}/sass/libs.sass`,
    dest: `${to}/css`,
});

lazyRequireTask('css:styles', './tasks/sass.js', {
    src: [`${from}/sass/app.sass`],
    dest: `${to}/css`,
});

gulp.task('app:styles', 
    gulp.series(
        'lib:styles',
        'css:styles',
    ) 
);

/************************/

/*************GET SCRIPTS START*************/
lazyRequireTask('app:scripts', './tasks/scripts.js', {
    src: `${from}/js/app.js`,
    dest: `${to}/js`
});

/************************/

/*************GET TEMPLATE START*************/
lazyRequireTask('app:tpl', './tasks/haml.js', {
    src: `./src/*.haml`,
    dest: `./dist`,
});

lazyRequireTask('tpl:debug', './tasks/haml.debug.js', {
    src: `./src/**/*.haml`,
    dest: `./dist`,
});

/************************/

/*************CLEAN -> BUILD -> SERVER  START*************/
lazyRequireTask('clean', './tasks/clean.js', {
    del: [
        './dist', 
        './src/assets/sass/base/_fonts.icons.scss',
        './src/assets/sass/base/sprite.scss',
        './manifest.json'
    ],
});

gulp.task('build', 
    gulp.series(
        //'tpl:debug'
        'clean',
        gulp.parallel(
            'app:media',
        ),
        'app:styles',
        'app:scripts',
        'app:tpl'
    ) 
);

lazyRequireTask('server', './tasks/reload.js', {
    server: './dist',
    allFiles: './dist/**/*.*'
});

/************************/

gulp.task('watch', function(){

    /*************MEDIA WATCH*************/

    gaze(`${from}/ico/ico.png`, function(err, watcher) {
       this.on('all', gulp.series('ico'));
    });
    
    gaze(`${from}/img/**/*.{png,jpg,gif}`, function(err, watcher) {
       this.on('all', gulp.series('img'));
    });

    gaze(`${from}/img/**/*.svg`, function(err, watcher) {
       this.on('all', gulp.series('img:svg'));
    });

    gaze(`${from}/sprites-svg/**/*`, function(err, watcher) {
       this.on('all', gulp.series('sprite:clean', 'sprite:svg', 'css:styles'));
    });
    
    gaze(`${from}/pictures/**/*.{png,jpg,gif}`, function(err, watcher) {
       this.on('all', gulp.series('pictures'));
    });

    gaze(`${from}/pictures/**/*.svg`, function(err, watcher) {
       this.on('all', gulp.series('pictures:svg'));
    });
    gaze(`${from}/media/**/*`, function(err, watcher) {
       this.on('all', gulp.series('media'));
    });
    gaze(`${from}/logos/**/*`, function(err, watcher) {
       this.on('all', gulp.series('logos'));
    });

    gaze(`${from}/fonts/**/*`, function(err, watcher) {
       this.on('all', gulp.series('fonts:local'));
    });

    gaze(`${from}/icons/**/*`, function(err, watcher) {
       this.on('all', gulp.series('fonts:icons', 'css:styles'));
    });


    /*************STYLES WATCH*************/
    
    gaze(`${from}/sass/libs.sass`, function(err, watcher) {
       this.on('all', gulp.series('lib:styles'));
    });

    gaze([`${from}/sass/**/*.sass`, `!${from}/sass/**/libs.sass`], function(err, watcher) {
       this.on('all', gulp.series('css:styles'));
    });

    /*************SCRIPTS WATCH*************/

    gaze(`${from}/js/**/*.js`, function(err, watcher) {
       this.on('all', gulp.series('app:scripts'));
    });

    /*************TEMPLATE WATCH*************/

    // gaze(`./src/**/*.haml`, function(err, watcher) {
    //   this.on('all', gulp.series('app:tpl'));
    // });

    gulp.watch(['./src/*.haml', './src/components/*.haml'], gulp.series('app:tpl'));
});


gulp.task('default',  
    gulp.series(
        'build', 
        gulp.parallel('watch', 'server')
    )
);