'use strict'

const gulp = require('gulp');
const $    = require('gulp-load-plugins')();


const Dotenv = require('dotenv-webpack');
require('dotenv').config()
let isDev = !!process.env.DEBUG;

module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
	        .pipe($.cached(options.taskName))
	        .pipe($.responsiveImages({
	        	'ico.png': [
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    rename: 'fav/favicon-16x16.png',
	                    width: "16",
	                },
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    rename: 'fav/favicon-32x32.png',
	                    width: "32",
	                },
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    rename: 'fav/favicon-96x96.png',
	                    width: "96",
	                },
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    rename: 'ati/ati-57x57.png',
	                    width: "57",
	                },
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    rename: 'ati/ati-60x60.png',
	                    width: "60",
	                },
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    rename: 'ati/ati-72x72.png',
	                    width: "72",
	                },
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    rename: 'ati/ati-76x76.png',
	                    width: "76",
	                },
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    rename: 'ati/ati-114x114.png',
	                    width: "114",
	                },
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    rename: 'ati/ati-120x120.png',
	                    width: "96",
	                },
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    rename: 'ati/ati-144x144.png',
	                    width: "144",
	                },
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    rename: 'ati/ati-152x152.png',
	                    width: "152",
	                },
	            ]
	        }))
	        .pipe($.if(!process.env.hasOwnProperty('DEBUG'), $.rev()))
	        .pipe(gulp.dest(options.dest))
			.pipe($.if(!process.env.hasOwnProperty('DEBUG'), $.rev.manifest({
				path: 'manifest.json',
            	merge: true 
        	})))
        	.pipe($.if(!process.env.hasOwnProperty('DEBUG'), gulp.dest('./')))
	};
};