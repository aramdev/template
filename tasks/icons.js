'use strict'

const gulp = require('gulp');
const $    = require('gulp-load-plugins')();


const Dotenv = require('dotenv-webpack');
require('dotenv').config()

const runTimestamp = Math.round(Date.now()/1000);
const fontName = !process.env.hasOwnProperty('DEBUG') ? `fonts.icons-${runTimestamp}` : 'fonts.icons'


module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
	        .pipe($.iconfontCss({
	            fontPath: '../fonts/',
	            fontName: fontName,
	            path: options.path,
	            targetPath: '../../sass/base/_fonts.icons.scss',
	            cssClass: "icn",
	        }))
	        .pipe($.iconfont({
	            fontName: fontName,
	            formats: ['eot', 'svg', 'ttf', 'woff', "woff2"],
	            normalize:true,
	            prependUnicode: true,
	            fontHeight: 1000,
	        }))
	        .pipe(gulp.dest(function(file){
				return file.extname != '.scss' ? `${options.dest}` : './src/assets/sass/base'
			}))
	};
};

    