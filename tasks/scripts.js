'use strict'

const gulp     = require('gulp');
const $        = require('gulp-load-plugins')();


const webpack = require('webpack'); 
const webpackStream = require('webpack-stream');
const compiler = require('webpack')


const Dotenv = require('dotenv-webpack');
require('dotenv').config()

let isDev = !!process.env.DEBUG;
let isProd = !isDev;

let webConfig = {
	output: {
		filename: 'app.js'
	},
	performance: { hints: false },
	module: {
		rules: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: '/node_modules/'
			}
		]
	},
	plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
		new Dotenv()
    ],
	mode: isDev ? 'development' : 'production',
	devtool: isDev ? 'eval-source-map' : 'none'
}



module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
	        .pipe(webpackStream(webConfig, compiler))
			.pipe($.if(!process.env.hasOwnProperty('DEBUG'), $.rev()))
	        .pipe($.if(!process.env.hasOwnProperty('DEBUG'), $.revReplace({
				manifest: gulp.src('./manifest.json', {allowEmpty: true})
			})))
			.pipe(gulp.dest(options.dest))
			.pipe($.if(!process.env.hasOwnProperty('DEBUG'), $.rev.manifest({
				path: 'manifest.json',
            	merge: true 
        	})))
        	.pipe($.if(!process.env.hasOwnProperty('DEBUG'), gulp.dest('./')))
	};
};



