'use strict'

const gulp = require('gulp');
const $    = require('gulp-load-plugins')();



const Dotenv = require('dotenv-webpack');
require('dotenv').config()
let isDev = !!process.env.DEBUG;



module.exports = function(options) {
	return function(){
		
		return gulp.src(options.src)
			.pipe($.include())
			.pipe($.cached(options.taskName))
	        .pipe($.rubyHaml({
	            doubleQuote: true, 
	            encodings: "UTF-8",
	            unixNewlines: true,
	            trace: true
	        }).on('error', function(e) { console.log(e.message); }))
			.pipe($.cached(options.taskName))
			.pipe($.if(!process.env.hasOwnProperty('DEBUG'), $.revReplace({
				manifest: gulp.src('./manifest.json', {allowEmpty: true})
			})))
	        .pipe($.htmlBeautify({
	            indent_char: '\t', 
	            indent_size: 1
	        }))
	        .pipe(gulp.dest(options.dest));
	};
};




