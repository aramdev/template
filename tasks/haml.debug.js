'use strict'

const gulp = require('gulp');
const $    = require('gulp-load-plugins')();



const Dotenv = require('dotenv-webpack');
require('dotenv').config()
let isDev = !!process.env.DEBUG;



module.exports = function(options) {
	return function(){
		
		return gulp.src(options.src)
			.pipe($.rubyHaml({
	            doubleQuote: true, 
	            encodings: "UTF-8",
	            unixNewlines: true,
	            trace: true
	        }, {eval: false}))
	        // .on('error', function(e) { console.log(e); }))
	        .on('error', function(err) {
	        	console.log(err.message);
	        	console.log('====================');
		    	console.error(err.stack) 
		    })
			.pipe(gulp.dest(options.dest));
	};
};




