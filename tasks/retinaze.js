'use strict'

const gulp = require('gulp');
const $    = require('gulp-load-plugins')();

const Dotenv = require('dotenv-webpack');
require('dotenv').config()
let isDev = !!process.env.DEBUG;

module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
	        .pipe($.cached(options.taskName))
	        .pipe($.responsiveImages({
	        	'**/*.{png,jpg,gif}': [
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    suffix: '@2x'
	                },
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    width: "50%"
	                }
	            ]
	        }))
	        .pipe($.if(!process.env.hasOwnProperty('DEBUG'), $.rev()))
	        .pipe(gulp.dest(options.dest))
	        .pipe($.if(!process.env.hasOwnProperty('DEBUG'), $.rev.manifest({
				path: 'manifest.json',
            	merge: true 
        	})))
        	.pipe($.if(!process.env.hasOwnProperty('DEBUG'), gulp.dest('./')))
	};
};
