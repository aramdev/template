'use strict'

const gulp = require('gulp');
const $    = require('gulp-load-plugins')();

const Dotenv = require('dotenv-webpack');
require('dotenv').config()
let isDev = !!process.env.DEBUG;


module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
			.pipe($.sass().on('error', $.sass.logError))
	        .pipe($.cached(options.taskName))
	        .pipe($.autoprefixer({
	        	overrideBrowserslist: ['> 99%', "last 20 version", 'firefox >= 4', 'IE 11'],
            	cascade: false
	        }))
	        .pipe($.groupCssMediaQueries())
	        .pipe($.uglifycss())
	        .pipe($.if(!process.env.hasOwnProperty('DEBUG'), $.revReplace({
				manifest: gulp.src('./manifest.json', {allowEmpty: true})
			})))
	        .pipe($.if(!process.env.hasOwnProperty('DEBUG'), $.rev()))
	        .pipe(gulp.dest(options.dest))
	        .pipe($.if(!process.env.hasOwnProperty('DEBUG'), $.rev.manifest({
				path: 'manifest.json',
            	merge: true 
        	})))
        	.pipe($.if(!process.env.hasOwnProperty('DEBUG'), gulp.dest('./')))
	};
};



			