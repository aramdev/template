import $ from 'jquery'
import 'bootstrap'
import 'owl.carousel.es6'

// MODULES
import autosize from './modules/autosize.js'
import formValidate from './modules/form.validate.js'
import module from './modules/module.js'


$(function(){
	autosize.init()
	// formValidate.init()
	// module.init()

	$(".someFm").each(function (index) {
		// (new Form(this)).validation()
		$(this).validate({  
			errorClass: 'invalid',
			errorPlacement: function(error, element) {
				let $place = element.parents('.field-case').find('.field-help')
				error.appendTo($place);
	
			},
			highlight: function ( element, errorClass, validClass ) {
				$( element )
					.parents( ".field-case" )
					.addClass( "invalid" )
					
			},
	
			unhighlight: function (element, errorClass, validClass) {
				$( element )
					.parents( ".field-case" )
					.removeClass( "invalid" );
			}
		})
	});

	
})