import $ from 'jquery'

const Module = (function(){
    // difinded elements for events
    let $body,
        $btn = '.button'
    
    // difinded elements for manipulatuon
    let $h1

    // return methods module
    return {
        caching () {
            $body = $('body')
            $h1 = $('h1')
        },
        runRedyDoc(){
            console.log('module')
        }, 
        eventForEl(){
            $h1.css({color: 'red'})
        },
        events(){
            $body.on('click', $btn, this.eventForEl.bind(this));
        },
        init(){
            this.caching()
            this.runRedyDoc()
            this.events()
        }
    }

})()

export default Module 

