import $ from 'jquery'
import validate from 'jquery-validation'
import "jquery-validation/dist/additional-methods.js"


export default class Form {
    constructor (form) {
        this.form = $(form)
    }
    validation(){
        let form = this.form.validate({  
            errorClass: 'invalid',
            errorPlacement: function(error, element) {
                let $place = element.parents('.field-case').find('.field-help')
                error.appendTo($place);

            },
            highlight: function ( element, errorClass, validClass ) {
                $( element )
                    .parents( ".field-case" )
                    .addClass( "invalid" )
                    
            },

            unhighlight: function (element, errorClass, validClass) {
                $( element )
                    .parents( ".field-case" )
                    .removeClass( "invalid" );
            }
        })
    }
}

